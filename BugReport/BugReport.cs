﻿using System;
using System.Collections.Specialized;
using System.Drawing;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Fabiogiopla.BugReport
{
	public class BugReport
	{
		/// <summary>
		/// Represents a checkbox the user may select to report a specific group of informations.
		/// </summary>
		public class Info
		{
			/// <summary>
			/// Content of this group of informations.
			/// </summary>
			public string Data = "";
			
			/// <summary>
			/// User-friendly title for this report.
			/// </summary>
			public string Title = "untitled";
			
			/// <summary>
			/// User-friendly description of this report.
			/// </summary>
			public string Description = null;
			
			/// <summary>
			/// Pre-selected by default.
			/// </summary>
			public bool Selected = false;
			
			/// <summary>
			/// Checkbox must be selected and cannot be disabled by the user.
			/// </summary>
			public bool Required = false;
			
			#region Info getters
			
			/// <summary>
			/// Get a group of informations regarding the system.
			/// </summary>
			/// <param name="selected">Pre-selected by default.</param>
			/// <param name="required">Checkbox must be selected and cannot be disabled by the user.</param>
			/// <returns>The info.</returns>
			public static Info GetSystemInfo(bool selected = false, bool required = false) {
				StringBuilder sb = new StringBuilder();
				sb.AppendFormat("Environment.CurrentDirectory: {0}\n", Environment.CurrentDirectory);
				sb.AppendFormat("Environment.ProcessorCount: {0}\n", Environment.ProcessorCount);

				return new Info() {
					Data = sb.ToString(),
					Title = "system info",
					Description = "Informations about your computer, some bugs may only happen on particular machines, reporting informations about your system may help with bugs specific to machines like yours.",
					Selected = selected,
					Required = required
				};
			}
			
			/// <summary>
			/// Get a group of informations regarding the .NET or Mono framework.
			/// </summary>
			/// <param name="selected">Pre-selected by default.</param>
			/// <param name="required">Checkbox must be selected and cannot be disabled by the user.</param>
			/// <returns>The info.</returns>
			public static Info GetFrameworkDetails(bool selected = false, bool required = false) {
				StringBuilder sb = new StringBuilder();
				sb.AppendFormat("Environment.Version: {0}\n", Environment.Version);
				sb.AppendFormat("Environment.OSVersion: {0}\n", Environment.OSVersion);
				sb.AppendFormat("Environment.Is64BitProcess: {0}\n", Environment.Is64BitProcess);
				sb.AppendFormat("Environment.Is64BitOperatingSystem: {0}\n", Environment.Is64BitOperatingSystem);
				sb.AppendFormat("Mono.Runtime defined: {0}\n", Type.GetType("Mono.Runtime") != null ? bool.TrueString : bool.FalseString);

				return new Info() {
					Data = sb.ToString(),
					Title = "framework details",
					Description = "The framework is the engine that runs this application, sometimes errors may only happen in specific frameworks, reporting the version of your framework will help with bugs that only happen when the application is run in frameworks like the one running on your machine.",
					Selected = selected,
					Required = required
				};
			}
			
			/// <summary>
			/// Build a group of informations from an exception.
			/// </summary>
			/// <param name="e">The exception to extract informations from.</param>
			/// <param name="selected">Pre-selected by default.</param>
			/// <param name="required">Checkbox must be selected and cannot be disabled by the user.</param>
			/// <returns>The info.</returns>
			public static Info GetStacktrace(Exception e, bool selected = false, bool required = false) {
				return new Info() {
					Data = (e == null ? "no stacktrace provided" : e.ToString()),
					Title = "stacktrace",
					Description = "In computing, a stack trace is a report of the active stack frames at a certain point in time during the execution of a program.\nProgrammers commonly use stack tracing during interactive and post-mortem debugging.\nEnd-users may see a stack trace displayed as part of an error message, which the user can then report to a programmer.\nA stack trace allows tracking the sequence of nested functions called - up to the point where the stack trace is generated. In a post-mortem scenario this extends up to the function where the failure occurred (but was not necessarily caused).",
					Selected = selected,
					Required = required
				};
			}
			
			/// <summary>
			/// Build a group of informations from a log.
			/// </summary>
			/// <param name="log">The log to extract informations from.</param>
			/// <param name="selected">Pre-selected by default.</param>
			/// <param name="required">Checkbox must be selected and cannot be disabled by the user.</param>
			/// <returns>The info.</returns>
			public static Info GetApplicationLog(string log, bool selected = false, bool required = false) {
				return new Info() {
					Data = (log == null ? "no application log provided" : log),
					Title = "application log",
					Description = "In computing, a log is a file that records either the events which happen while software runs.\nEvent logs record events taking place in the execution of a system in order to provide an audit trail that can be used to understand the activity of the system and to diagnose problems. They are essential to understand the activities of complex systems.",
					Selected = selected,
					Required = required
				};
			}
			#endregion
		}
		
		/// <summary>
		/// Title of the application the bug is being reported for.
		/// </summary>
		public string ApplicationTitle = "MyApplication";
		
		/// <summary>
		/// Initial text in personal message field.
		/// </summary>
		public string DefaultMessage = "-- What happened? --\nDescribe the bug that you noticed (if any).\n\n-- What did you do to make the error happen? --\nIf possible, list the sequence of actions that causes the bug.\n\n-- What should have happened instead? --\nIf you think something else should have happened, write it here.\n\n-- Contact informations --\nIf you want to be contacted please leave your e-mail address here.\n";
		
		/// <summary>
		/// Each of these options represents a checkbox the user may select to report a specific group of informations.
		/// </summary>
		public Info[] Infos = new Info[0];
		
		/// <summary>
		/// Display a bug-reporting form.
		/// </summary>
		/// <param name="bugReportUrl">URL to send the bug report to.</param>
		/// <param name="visualStyles">Enable visual styles if available to use system theme.</param>
		/// <returns>True if report has been reported correctly.</returns>
		public bool Report(string bugReportUrl, bool visualStyles = true) {
			if(visualStyles && !Application.RenderWithVisualStyles)
				Application.EnableVisualStyles();
			
			var bugReportForm = new BugReportForm(bugReportUrl, this);
			Application.Run(bugReportForm);
			return bugReportForm.ReportSent;
		}
		
		/// <summary>
		/// Display a bug-reporting form.
		/// </summary>
		/// <param name="bugReportUrl">URL to send the bug report to.</param>
		/// <param name="application">Title of the application the bug is being reported for.</param>
		/// <param name="e">Exception thrown to detect the bug.</param>
		/// <param name="log">Application log.</param>
		/// <returns>True if report has been reported correctly.</returns>
		public static bool ReportBug(string bugReportUrl, string application = null, Exception e = null, string log = null) {
			var report = new BugReport() {
				Infos = new BugReport.Info[] {
					BugReport.Info.GetApplicationLog(log, true, true),
					BugReport.Info.GetStacktrace(e, true, true),
					BugReport.Info.GetFrameworkDetails(true),
					BugReport.Info.GetSystemInfo(false)
				}
			};
			
			if(application != null)
				report.ApplicationTitle = application;
			
			return report.Report(bugReportUrl);
		}
		
		#region Bug report form
		internal class BugReportForm : Form
		{
			private const int FormWidth = 600;
			private const int InfoFormWidth = 500;
			private const int Spacing = 12;
			private const int LineHeight = 13;
			private const int CheckboxSpacing = 6;
			private const int CheckboxHeight = 16;
			private const int LinkLabelWidth = 120;
			private const int MessageLines = 12;
			private const int InfoDescriptionLines = 5;
			private const int InfoDataLines = 12;
			private const int ButtonWidth = 120;
			private const int ButtonHeight = 40;
			
			#region AboutReportForm
			internal class AboutReportForm : Form
			{
				public AboutReportForm(BugReport.Info info) {
					initializeComponent(info);
				}

				private void initializeComponent(BugReport.Info info) {
					SuspendLayout();

					FormBorderStyle = FormBorderStyle.FixedDialog;
					StartPosition = FormStartPosition.CenterScreen;
					TopMost = true;
					MaximizeBox = false;
					MinimizeBox = false;
					ShowInTaskbar = false;
					Text = string.Format("What is {0}?", escapeNewLines(info.Title));

					int nextPos = Spacing;
					if(info.Description != null) {
						var desc = new Label();
						desc.Location = new Point(Spacing, nextPos);
						desc.Text = escapeNewLines(info.Description);

						int width = InfoFormWidth - Spacing * 2;
						int height;	// = LineHeight*InfoDescriptionLines;
						using(Graphics g = CreateGraphics())
							height = (int) Math.Ceiling(g.MeasureString(desc.Text, desc.Font, width).Height);

						desc.Size = new Size(width, height);
						Controls.Add(desc);

						nextPos += desc.Size.Height + Spacing;
					}

					var data = new TextBox();
					data.Multiline = true;
					data.ScrollBars = ScrollBars.Vertical;
					data.Location = new Point(Spacing, nextPos);
					data.Size = new Size(InfoFormWidth - Spacing * 2, LineHeight * InfoDataLines + 6);	// 6 for the border and padding
					data.Text = escapeNewLines(info.Data);
					data.ReadOnly = true;
					data.BackColor = SystemColors.Window;
					data.Select(0, 0);
					Controls.Add(data);

					ClientSize = new Size(InfoFormWidth, data.Location.Y + data.Size.Height + Spacing);

					ResumeLayout(true);
				}
			}
			#endregion
			
			private CheckBox[] checkbox;
			private CheckBox messageCheckbox;
			private TextBox messagebox;
			
			public bool ReportSent { get; private set; }
			
			public BugReportForm(string bugReportUrl, BugReport report) {
				initializeComponent(bugReportUrl, report);
			}

			private string buildReport(BugReport report) {
				StringBuilder reportmsg = new StringBuilder();

				for(int i = 0; i < report.Infos.Length; i++) {
					if(checkbox[i].Checked) {
						var info = report.Infos[i];
						reportmsg.AppendFormat("~~~ {0} ~~~\n{1}\n\n", info.Title, info.Data);
					}
				}
				
				if(messageCheckbox.Checked)
					reportmsg.AppendFormat("~~~ user message ~~~\n{0}\n\n", messagebox.Text);

				return reportmsg.ToString();
			}

			private void reportBug(string bugReportUrl, BugReport report) {
				Hide();

				var thread = new Thread(() => {
					try {
						using(var client = new WebClient()) {
							var values = new NameValueCollection();
							values["message"] = buildReport(report);
							client.UploadValues(string.Format(bugReportUrl, report.ApplicationTitle), values);
						}

						ReportSent = true;

						Invoke((MethodInvoker) delegate {
							MessageBox.Show(this, "Thanks for reporting this bug!", "Bug reported successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
							Close();
						});
					} catch(Exception) {
						Invoke((MethodInvoker) delegate {
							var response = MessageBox.Show(this, "Unable to send your bug report to the developer!", "Failed to report bug", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
							if(response == DialogResult.OK)
								Show();
							else
								Close();
						});
					}
				});
				thread.Start();
			}

			private void initializeComponent(string bugReportUrl, BugReport report) {
				SuspendLayout();

				FormBorderStyle = FormBorderStyle.FixedDialog;
				StartPosition = FormStartPosition.CenterScreen;
				TopMost = true;
				MaximizeBox = false;
				MinimizeBox = false;
				ShowInTaskbar = false;
				Text = string.Format("{0} - Report Bug", escapeNewLines(report.ApplicationTitle));

				var message = new Label();
				message.Location = new Point(Spacing, Spacing);
				message.Size = new Size(FormWidth - Spacing * 2, LineHeight * 2);
				message.Text = string.Format("Something bad happened while {0} was running, sending a bug report might help fixing this problem.\nDo you want to send a report?", report.ApplicationTitle);
				Controls.Add(message);

				checkbox = new CheckBox[report.Infos.Length + 1];

				int nextPos = message.Location.Y + message.Size.Height + Spacing;
				for(int i = 0; i < report.Infos.Length; i++) {
					var info = report.Infos[i];

					var cb = checkbox[i] = new CheckBox();
					cb.Location = new Point(Spacing + 4, nextPos);
					cb.Size = new Size(FormWidth - Spacing * 3 - LinkLabelWidth, CheckboxHeight);
					cb.Text = string.Format("Include {0}", escapeNewLines(info.Title));
					cb.Checked = info.Selected || info.Required;
					cb.Enabled = !info.Required;
					Controls.Add(cb);

					var link = new LinkLabel();
					link.TextAlign = ContentAlignment.MiddleRight;
					link.Location = new Point(FormWidth - Spacing - LinkLabelWidth, nextPos);
					link.Size = new Size(LinkLabelWidth, CheckboxHeight);
					link.Text = "What's this?";
					link.LinkClicked += delegate {
						new AboutReportForm(info).ShowDialog(this);
					};
					Controls.Add(link);

					nextPos += cb.Size.Height + CheckboxSpacing;
				}

				messageCheckbox = checkbox[report.Infos.Length - 1] = new CheckBox();
				messageCheckbox.Location = new Point(Spacing + 4, nextPos);	// 4 to indent a little
				messageCheckbox.Size = new Size(FormWidth - Spacing * 2, CheckboxHeight);
				messageCheckbox.Text = "Include personal message";
				messageCheckbox.CheckedChanged += delegate { messagebox.Enabled = messageCheckbox.Checked; };
				Controls.Add(messageCheckbox);

				messagebox = new TextBox();
				messagebox.Multiline = true;
				messagebox.ScrollBars = ScrollBars.Vertical;
				messagebox.Location = new Point(Spacing, messageCheckbox.Location.Y + messageCheckbox.Size.Height + Spacing);
				messagebox.Size = new Size(FormWidth - Spacing * 2, LineHeight * MessageLines + 6);	// 6 for the border and padding
				messagebox.Text = escapeNewLines(report.DefaultMessage);
				messagebox.Enabled = false;
				Controls.Add(messagebox);

				var sendbtn = new Button();
				sendbtn.Location = new Point(FormWidth - ButtonWidth - Spacing, messagebox.Location.Y + messagebox.Size.Height + Spacing);
				sendbtn.Size = new Size(ButtonWidth, ButtonHeight);
				sendbtn.Text = "Report";
				sendbtn.TabIndex = 0;
				sendbtn.Click += delegate {
					reportBug(bugReportUrl, report);
				};
				Controls.Add(sendbtn);

				var cancelbtn = new Button();
				cancelbtn.Location = new Point(sendbtn.Location.X - ButtonWidth - Spacing, messagebox.Location.Y + messagebox.Size.Height + Spacing);
				cancelbtn.Size = new Size(ButtonWidth, ButtonHeight);
				cancelbtn.Text = "Cancel";
				cancelbtn.Click += delegate {
					this.Close();
				};
				Controls.Add(cancelbtn);

				ClientSize = new Size(FormWidth, sendbtn.Location.Y + sendbtn.Size.Height + Spacing);

				ResumeLayout(true);
			}

			private static string escapeNewLines(string str) {
				return str == null ? null : str.Replace("\n", Environment.NewLine);
			}
		}
		#endregion
	}
}
