﻿#region Using directives

using System;
using System.Reflection;
using System.Runtime.InteropServices;

#endregion

// associated with an assembly.
[assembly: AssemblyTitle("Fabiogiopla.BugReport")]
[assembly: AssemblyDescription("Report bugs with a user-friendly interface.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fabio Iotti")]
[assembly: AssemblyProduct("BugReport")]
[assembly: AssemblyCopyright("Copyright (c) 2014 Fabio Iotti")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.*")]
