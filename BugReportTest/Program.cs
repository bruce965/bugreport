﻿using System;
using Fabiogiopla.BugReport;

namespace BugReporter
{
	class Program
	{
		public static void Main(string[] args)
		{
			var reported = BugReport.ReportBug("http://example.com/services/bugreport/report.php?app={0}");
			
			Console.WriteLine("{0}reported", reported ? "" : "not ");
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}