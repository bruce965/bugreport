<?php

$bugreport_destination = 'your.mail@example.com';

if(!isset($_POST['message']))
	die("This system is intended to be used as a service!");

$application = isset($_GET['app']) ? $_GET['app'] : "Unspecified Source";
$address = "Reported from: ".$_SERVER['REMOTE_ADDR']."\n\n";
$message = wordwrap($_POST['message'], 70);

mail($bugreport_destination, "Bug Report APIs (".$application.")", $address.$message);

echo 'OK';

?>